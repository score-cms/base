<?php

namespace Score\BaseBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 */
class BaseEntity {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="visibility", type="boolean", nullable=true)
     */
    protected $visibility = true;

    /**
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    protected $isDeleted = false;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="edited_at", type="datetime")
     */
    protected $editedAt;

    /**
     * @ORM\PrePersist()
     */
    public function preCreated() {
        $this->createdAt = new DateTime('now');
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preEdited() {
        $this->editedAt = new DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function getHash() {
        $start = substr(sha1($this->getId()), 0, 8);
        $end = substr(md5(time()), 0, 8);
        $id = ($this->getId() * 4) + 128;

        return $start . $id . $end;
    }


    function getCreatedAt() {
        return $this->createdAt;
    }

    function getEditedAt() {
        return $this->editedAt;
    }

    function getVisibility() {
        return $this->visibility;
    }

    function setVisibility($visibility): self {
        $this->visibility = $visibility;

        return $this;
    }

    function getIsDeleted() {
        return $this->isDeleted;
    }

    function setIsDeleted($isDeleted): self {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
