<?php

namespace Score\BaseBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator as Paginator;

class Datagrid {

    protected $entityManager;
    protected $router;
    protected $cols;
    protected $pagerStart;
    protected $pagerLength;
    protected $entityName;
    protected $orderParams;
    protected $searchCriteria;


    public function __construct($em, $entityName,$router)
    {
        $this->entityManager = $em;
        $this->entityName = $entityName;
        $this->router = $router;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function getCols()
    {
        return $this->cols;
    }

    public function getPagerStart()
    {
        return $this->pagerStart;
    }

    public function getPagerLength()
    {
        return $this->pagerLength;
    }

    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function setCols($cols)
    {
        $this->cols = $cols;
    }

    public function setPagerStart($pagerStart)
    {
        $this->pagerStart = $pagerStart;
    }

    public function setPagerLength($pagerLength)
    {
        $this->pagerLength = $pagerLength;
    }

    public function getEntityName()
    {
        return $this->entityName;
    }

    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    public function getOrderParams()
    {
        return $this->orderParams;
    }

    public function setOrderParams($orderParams)
    {
        $this->orderParams = $orderParams;
    }

    public  function getRouter() {
return $this->router;
}

public  function setRouter($router) {
$this->router = $router;
}



    public function buildDataAsArray($paginator)
    {
        $data = array();
        foreach ($paginator as $object)
        {
            $data[] = array(
                '<a href="">' . $object->getId() . '</a>',
                $object->getName()
            );
        }
        return $data;
    }

    public function getRecordsAsArray()
    {
        $requestOrderCriteria = $this->getOrderParams();
        $cols = $this->getCols();
        $search = $this->getSearchCriteria();

        $em = $this->getEntityManager();
        $repo =  $em->getRepository($this->getEntityName());
        $qd = $repo->createQueryBuilder('a');
        $qd->add('orderBy', $cols[$requestOrderCriteria[0]['column']].' '.$requestOrderCriteria[0]['dir']);

        if(null != $search)
        {
            $qd->andWhere('a.search like :search'); 
            $qd->setParameter('search', '%'.$search.'%');      
        }

        $qd->setFirstResult($this->getPagerStart());
        $qd->setMaxResults($this->getPagerLength());
        $query = $qd->getQuery();

        $paginator = new Paginator($query, $fetchJoinCollection = true);
        $data = $this->buildDataAsArray($paginator);

        return $data;
    }

    public function getRecordsFiltered()
    {
        $recordsTotalQuery = $this->getEntityManager()->createQuery('SELECT count(a.id)  FROM ' . $this->getEntityName() . ' a ')->getOneOrNullResult();
        $recordsTotal = (int)$recordsTotalQuery[1];
        return $recordsTotal;
    }

    public function getRecordsTotal()
    {
        $recordsTotalQuery = $this->getEntityManager()->createQuery('SELECT count(a.id)  FROM ' . $this->getEntityName() . ' a ')->getOneOrNullResult();
        $recordsTotal = (int)$recordsTotalQuery[1];
        return $recordsTotal;
    }

    /**
     * Get the value of searchCriteria
     */ 
    public function getSearchCriteria()
    {
        return $this->searchCriteria;
    }

    /**
     * Set the value of searchCriteria
     *
     * @return  self
     */ 
    public function setSearchCriteria($searchCriteria)
    {
        $this->searchCriteria = $searchCriteria;

        return $this;
    }
}
