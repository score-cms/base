<?php

namespace Score\BaseBundle\Twig;

use Score\BaseBundle\Services\ReactAppLoader;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    private $reactAppLoader;
    private $version;

    public function __construct(ReactAppLoader $reactAppLoader, $version)
    {
        $this->reactAppLoader = $reactAppLoader;
        $this->version = $version;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function loadReactApps(Array $apps = [])
    {
        return $this->reactAppLoader->findAppsByBlockNames($apps);
    }

    public function loadReactAppFiles($extension, $pathToManifest)
    {
        return $this->reactAppLoader->getFilePaths($extension, $pathToManifest);
    }
    
    public function loadReactAppFilesDist($pathToManifest, $extension = null)
    {
        return $this->reactAppLoader->getFilePathsDist($pathToManifest, $extension);
    }
}