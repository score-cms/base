<?php
// src/Twig/AppExtension.php
namespace Score\BaseBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Score\BaseBundle\Twig\AppRuntime;

class AppExtension extends AbstractExtension
{

    // public function getFilters()
    // {
    //     return [
    //         new TwigFilter('price', [$this, 'formatPrice']),
    //     ];
    // }

    public function getFunctions()
    {
        return [
            new TwigFunction('cms_get_version', [AppRuntime::class, 'getVersion']),
            new TwigFunction('cms_react_apps', [AppRuntime::class, 'loadReactApps']),
            new TwigFunction('cms_react_app_part', [AppRuntime::class, 'loadReactAppFiles']),
            new TwigFunction('cms_react_app_part_dist', [AppRuntime::class, 'loadReactAppFilesDist'])
        ];
    }
}


