<?php

namespace Score\BaseBundle\Services;

class Pager
{
    private $items_per_page = 10;
    private $pagging_length = 5;
    private $count;
    private $page;
    private $last_page;
    private $url;

    //private $query;

    public function setItemsPerPage($val)
    {
        $this->items_per_page = $val;
    }

    public function setCount($val)
    {
        $this->count = $val;
    }

    public function setPagingLength($val)
    {
        $this->pagging_length = $val;
    }

    public function setPage($val)
    {
        if (null != $val) {
            $this->page = $val;
        } else {
            $this->page = 1;
        }
    }

    public function setLastPage($val)
    {
        $this->last_page = $val;
    }

    public function getItemsPerPage()
    {
        return $this->items_per_page;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getPaggingLength()
    {
        return $this->pagging_length;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getLastPage()
    {
        return $this->last_page;
    }

    public function setUrl($val)
    {
        return $this->url = $val;
    }

    public function getUrl()
    {
        $urlParts = parse_url($this->url);
        $params = array();
        if (array_key_exists('query', $urlParts)) {
            parse_str($urlParts['query'], $params);
            unset($params['p']);
        }
        return $urlParts['path'] . '?' . http_build_query($params);;
    }


    public function getPages()
    {
        $current_pager = array();
        $pages = ceil($this->getCount() / $this->getItemsPerPage());
        $this->setLastPage($pages);
        $start = $this->getPage() - floor($this->getPaggingLength() / 2);

        if ($start < 1 || $start < 0) {
            $start = 1;
        } elseif ($this->getPage() == $pages) {
            $start = $pages - $this->getPaggingLength() + 1;
            if ($start <= 0) {
                $start = 1;
            }
        }

        $max = $start + $this->getPaggingLength();

        if ($max > $pages) {
            $max = $pages + 1;
        }

        for ($i = $start; $i < $max; $i++) {
            $current_pager[] = $i;
        }

        return $current_pager;

    }

    public function getPreviousPage()
    {
        if (0 < $this->page - 1) {
            return $this->page - 1;
        } else {
            return 1;
        }
    }

    public function getNextPage()
    {
        if ($this->getLastPage() <= $this->page) {
            return $this->getLastPage();
        } else {
            return $this->page + 1;
        }
    }

    public function getPreviousPageLink()
    {
        return $this->getUrl() . '&p=' . $this->getPreviousPage();
    }

    public function getlastPageLink()
    {
        return $this->getUrl() . '&p=' . $this->getLastPage();
    }

    public function nextPageLink()
    {
        return $this->getUrl() . '&p=' . $this->getNextPage();
    }

    public function getNumberPageLink($page)
    {
        return $this->getUrl() . '&p=' . $page;
    }
}
