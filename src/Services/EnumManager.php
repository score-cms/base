<?php

namespace Score\BaseBundle\Services;

class EnumManager
{
    /*
     * Storage provider
     */

    protected $dbProvider;

    function __construct($em)
    {
        $this->dbProvider = $em;
    }

    public function getDbProvider()
    {
        return $this->dbProvider;
    }

    public function setDbProvider($dbProvider)
    {
        $this->dbProvider = $dbProvider;
    }

    public function getRegionFormChoices()
    {
        $conn = $this->getDbProvider()->getConnection();
        $data = $conn->fetchAll('SELECT * FROM ENUM_REGION');
        $list = array();
        foreach ($data as $d) {
            $list[$d['NAZKR']] = $d['IDKR'];
        }

        return $list;
    }

    public function getDistrictFormChoices($regionId)
    {
        $conn = $this->getDbProvider()->getConnection();
        $data = $conn->fetchAll('SELECT * FROM ENUM_DISTRICT WHERE ID_KRAJ = ?', array($regionId));
        $list = array();
        foreach ($data as $d) {
            $list[$d['NAZOKS']] = $d['ID_OKRES'];
        }

        return $list;
    }

    public function getCityFormChoices($districtId)
    {
        $conn = $this->getDbProvider()->getConnection();
        $data = $conn->fetchAll('SELECT * FROM ENUM_CITY WHERE DISTRICT_ID = ?', array($districtId));
        $list = array();
        foreach ($data as $d) {
            $list[$d['TITLE']] = $d['ID'];
        }

        return $list;
    }

}
