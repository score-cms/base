<?php

namespace Score\BaseBundle\Services;

use Imagine\Image\Box;
use Imagine\Gd\Imagine;
use Imagine\Image\ImageInterface;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;

class ImageManager
{

    private $saveOptions = [
        'resolution-units' => ImageInterface::RESOLUTION_PIXELSPERINCH,
        'resolution-x' => 300,
        'resolution-y' => 300,
        'jpeg_quality' => 75,
        'png_compression_level' => 9,
        'webp_quality' => 85
    ];

    public function createThumb($sourceImage, $targetImage, $params)
    {
        $imagine = new Imagine();
        $image = $imagine->open($sourceImage);
        $image->thumbnail(new Box($params['width'], $params['height']))->save($targetImage, array('jpeg_quality' => 85));
    }

    public  function resize($origFile, $newFile, $maxWidth = null, $maxHeight = null, $fitIn = false)
    {
        $imagine = new Imagine($origFile);
        $image = $imagine->open($origFile);
        if ($maxWidth || $maxHeight) {
            $imageSize = $image->getSize();
            if (!$maxHeight && $imageSize->getWidth() > $maxWidth) {
                $image->resize($imageSize->widen($maxWidth));
            } elseif (!$maxWidth && $imageSize->getHeight() > $maxHeight) {
                $image->resize($imageSize->heighten($maxHeight));
            } elseif ($maxWidth && $maxHeight) {
                if ($fitIn) {
                    $image->resize(new Box($maxWidth, $maxHeight));
                } else {
                    if ($imageSize->getWidth() > $imageSize->getHeight()) {
                        $image->resize($imageSize->widen($maxWidth));
                        if ($imageSize->getHeight() > $maxHeight) {
                            $image->resize($imageSize->heighten($maxHeight));
                        }
                    } else {
                        $image->resize($imageSize->heighten($maxHeight));
                        if ($imageSize->getWidth() > $maxWidth) {
                            $image->resize($imageSize->widen($maxWidth));
                        }
                    }
                }
            }
        }

        if ($newFile === null)
            return $image;

        return $image->save($newFile);
    }

    public function scaleTo($origFile, $newFile, $width = 100, $height = null)
    {
        if ($height === null)
            $height = $width;
        $imagine = new Imagine();
        $image = $imagine->open($origFile);
        if ($width && $height) {
            $size = new Box($width, $height);
        } else {
            $image_size = $image->getSize();
            $larger = max($image_size->getWidth(), $image_size->getHeight());
            $scale = max($width, $height);
            $size = $image_size->scale(1 / ($larger / $scale));
        }

        $mode = ImageInterface::THUMBNAIL_OUTBOUND;
        $image = $image->thumbnail($size, $mode);

        if ($newFile === null)
            return $image;

        return $image->save($newFile, $this->saveOptions);
    }
    
    public function thumbOver($origFile, $newFile, $width = 100, $height = null)
    {
        if ($height === null)
            $height = $width;

        $imagine = new Imagine();
        $image = $imagine->open($origFile);

        $palette = new RGB();
        $size = new Box($width, $height);
        $color = $palette->color('#ddd', 80);
        $imagec = $imagine->create($size, $color);

        $imaget = $image->thumbnail($size, ImageInterface::THUMBNAIL_OUTBOUND);
        $imageb = $image->thumbnail($size, ImageInterface::THUMBNAIL_OUTBOUND);
        $imagei = $image->thumbnail($size, ImageInterface::THUMBNAIL_INSET);
        $imageb->effects()->blur(3);
        $imaget->paste($imageb, new Point(0,0));
        $imaget->paste($imagec, new Point(0,0));
        $s = $imagei->getSize();
        $x = ($size->getWidth() - $s->getWidth()) / 2; 
        $y = ($size->getHeight() - $s->getHeight()) / 2; 
        $imaget->paste($imagei, new Point($x, $y));

        if ($newFile === null)
            return $imaget;

        return $imaget->save($newFile, $this->saveOptions);
    }
    
    /**
     * recursively create a long directory path
     */
    public function createPath($path)
    {
        if (is_dir($path))
            return true;
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1);
        $return = $this->createPath($prev_path);
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }

    public function getBaseRatio($x, $y, $round = true, $max = 10)
    {

        if (!$round) {
            $d = $this->__gcd($x, $y);
            $a2 = $x / $d;
            $b2 = $y / $d;
            return $a2 . "/" . $b2;
            
        } else {
            $r = [];

            $ok = $x > $y;
            $a0 = $ok ? $x : $y;
            $b0 = $ok ? $y : $x;

            for ($i = 1; $i <= $max; $i++) {
                $r0 = $a0 / $i;
                $r1 = $b0 / $r0;
                $r[] = [
                    "d" => abs($r1 - round($r1)),
                    "ratio" => $ok ? ($i . "/" . round($r1)) : (round($r1) . "/" . $i),
                ];
            }

            usort($r, function ($a1, $b1) {
                return $a1['d'] <=> $b1['d'];
            });

            //return $r;
            return $r[0]["ratio"];
        }
    }

    private function __gcd($a, $b)
    {
        if ($b == 0)
            return $a;
        return $this->__gcd($b, $a % $b);
    }
}
