<?php


namespace Score\BaseBundle\Services;

class AdjacencyTreeManager  extends BaseManager
{
    /*
     * Array of parents, usefull for breadcrumbing, path, etc.
     */
    private $parent_chain = array();
    private $scalar;


   function __construct($em)
   {
        $this->db_provider = $em;
   }

    public function setScalar($val)
    {
    	$this->scalar = $val;
    }

    public function setObject($val)
    {
        $this->object = $val;
    }

    public function getObject()
    {
        return $this->object;
    }

    public function findRoot()
    {
    	$root = $this->getRepository()->findOneBy(array('lvl' => 0, 'parentId' => 0));
    	return $root;
    }

    public function getParent($object = null)
    {
        if(null == $object)
        {
           $object =  $this->getObject();
        }

        $parent = $this->getRepository()->findOneBy(array('id' => $object->getParentId()));
        return $parent;
    }

    /**
     * @param object $object
     * @return array $parent_chain array of parent objects
     */
    public function getObjectParentChain($object = null)
    {
        $parent = $this->getParent($object);
        if(null != $parent)
        {
            $this->parent_chain[$parent->getId()] = $parent;
            $this->getObjectParentChain($parent);
        }
       return $this->parent_chain;
    }

    /**
     * @param mixed $id
     */
    public function hasChildren($id)
    {
		$count = $this->getRepository()->findBy(array('parentId' => $id));

        if('0' != count($count))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     *
     *
     * @param mixed $id
     * @return array
     */
    public function getChildren($id)
    {
        $children = array();

        $db_childs =  $this->getRepository()->createQueryBuilder('n')
            ->where('n.parentId = :parent_id')
            ->setParameter('parent_id', $id)
            ->orderBy('n.sortOrder', 'ASC')
            ->getQuery()
            ->getResult();

        foreach($db_childs as $row)
        {
            $children[] = $row;
        }

        return $children;
    }

     /**
     * Create multidimensional array
     *
     * @param mixed $id object id
     * @param $full_path array of brancha ids, parent ids + current id
     * @param $options options depth = depth of tree
     * @return array multidimensional array
     */
    public function buildBranchTree($id,$full_path,$options = array('depth' => 100))
    {
        $tmp = array();
        if($this->hasChildren($id))
        {
            $childs = $this->getChildren($id);
            foreach($childs as $child)
            {
                $tmp[$child->getId()]['node'] = $child;
                if($options['depth'] > $child->getLvl() && in_array($child->getId(),$full_path))
                {
                    if($this->hasChildren($child->getId()))
                    {
                         $tmp[$child->getId()]['children'] = $this->buildBranchTree($child->getId(),$full_path,$options);
                    }
                }
            }
        }
        return $tmp;
    }

    /**
     * Create multidimensional array
     *
     * @param mixed $id object id
     * @param $options options depth = depth of tree
     * @return array multidimensional array
     */
    public function buildTree($id,$options = array('depth' => 100))
    {
        $tmp = array();
        if($this->hasChildren($id))
        {
            $childs = $this->getChildren($id);
            foreach($childs as $child)
            {
                $tmp[$child->getId()]['node'] = $child;
                if($options['depth'] > $child->getLvl())
                {
                    if($this->hasChildren($child->getId()))
                    {
                         $tmp[$child->getId()]['children'] = $this->buildTree($child->getId(),$options);
                    }
                }
            }
        }
        return $tmp;
    }

     /**
     *
     *
     * @param mixed $id
     * @param mixed $parent_record
     * @return array
     */
    public function buildScalar($id,$parent_record = null)
    {
        if($this->hasChildren($id))
        {
            $childs = $this->getChildren($id);
            foreach($childs as $child)
            {
                $this->scalar[$child->getId()]['name'] = $child->getName();
                $this->scalar[$child->getId()]['node'] = $child;
                if($this->hasChildren($child->getId()))
                {
                     $this->buildScalar($child->getId());
                }
            }
        }
        return $this->scalar;
    }

     /**
     *
     * @param mixed $id
     * @param mixed $parent_record
     * @return array
     */
    protected $array;
    public function buildArray($id,$parent_record = null)
    {
        if($this->hasChildren($id))
        {
            $childs = $this->getChildren($id);
            foreach($childs as $child)
            {
                if($id != $child->getId())
                {
                    $this->array[$child->getId()] = $child;
                if($this->hasChildren($child->getId()))
                {
                     $this->buildArray($child->getId());
                }
                }


            }
        }
        return $this->array;
    }

    /**
     *
     *
     * @param mixed $id
     * @param mixed $parent_record
     * @return array
     */
    public function buildObjectScalar($id,$parent_record = null)
    {
    	if($this->hasChildren($id))
    	{
    		$childs = $this->getChildren($id);
    		$parent = $this->getRepository()->find($id);
    		foreach($childs as $child)
    		{
    			$child->setParent($parent);
    			$this->scalar[$child->getId()] = $child;
    			if($this->hasChildren($child->getId()))
    			{
    				$this->buildObjectScalar($child->getId());
    			}
    		}
    	}
    	return $this->scalar;
    }

    /**
     *
     * @param mixed $id
     * @param mixed $parent_record
     * @return array
     */
    private $plain_array;
    public function buildPlainArray($id,$parent_record = null)
    {
    	if($this->hasChildren($id))
    	{
    		$childs = $this->getChildren($id);
    		foreach($childs as $child)
    		{
    			$this->plain_array[] = $child->getName();
    			if($this->hasChildren($child->getId()))
    			{
    				$this->buildPlainArray($child->getId());
    			}
    		}
    	}
    	return $this->plain_array;
    }



}


