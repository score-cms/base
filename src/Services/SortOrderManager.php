<?php

namespace Score\BaseBundle\Services;
/**
 *
 */
class SortOrderManager extends BaseManager
{
    /**
     * Return max and min value of sort_order position
     */
    public function getLimits($criteria = array())
    {
        $criteria_string = $this->getCriteriaString($criteria);
        $qb = $this->getRepository()->createQueryBuilder('n');
        $query = $qb->select($qb->expr()->max('n.sort_order'), $qb->expr()->min('n.sort_order'))
            ->where('1 = 1 ' . $criteria_string);

        foreach ($criteria as $column_name => $column_value) {
            $query->setParameter($column_name, $column_value);
        }

        $limits = $query->getQuery()->getSingleResult();


        if (null == $limits[1]) {
            $limits[1] = 0;
        }

        if (null == $limits[2]) {
            $limits[2] = 0;
        }

        return $limits;
    }

    public function move($direction, $object, $criteria = array())
    {
        if ('up' == $direction) {
            $this->moveUp($object, $criteria);
        }

        if ('down' == $direction) {
            $this->moveDown($object, $criteria);
        }
    }

    public function moveUp($object, $criteria = array())
    {
        $criteria_string = $this->getCriteriaString($criteria);

        $upper_query = $this->getRepository()->createQueryBuilder('n')
            ->where('n.sort_order < :sort_order ' . $criteria_string)
            ->setParameter('sort_order', $object->getSortOrder());

        foreach ($criteria as $column_name => $column_value) {
            $upper_query->setParameter($column_name, $column_value);
        }

        $upper_node = $upper_query
            ->orderBy('n.sort_order', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getSingleResult();

        $upper_sort_order = $upper_node->getSortOrder();
        $upper_node->setSortOrder($object->getSortOrder());
        $object->setSortOrder($upper_sort_order);
        $this->saveSwitch($object, $upper_node);
    }

    public function moveDown($object, $criteria = array())
    {
        $criteria_string = $this->getCriteriaString($criteria);

        $down_query = $this->getRepository()->createQueryBuilder('n')
            ->where('n.sort_order > :sort_order ' . $criteria_string)
            ->setParameter('sort_order', $object->getSortOrder());

        foreach ($criteria as $column_name => $column_value) {
            $down_query->setParameter($column_name, $column_value);
        }

        $down_node = $down_query->orderBy('n.sort_order', 'ASC')
            ->getQuery()
            ->setMaxResults(1)
            ->getSingleResult();

        $down_sort_order = $down_node->getSortOrder();
        $down_node->setSortOrder($object->getSortOrder());
        $object->setSortOrder($down_sort_order);
        $this->saveSwitch($object, $down_node);
    }

    private function getCriteriaString($criteria)
    {
        $criteria_string = '';
        if (!empty($criteria)) {
            $criteria_string = ' ';
            foreach ($criteria as $column_name => $column_value) {
                $criteria_string .= ' AND n.' . $column_name . ' =:' . $column_name;
            }
        }

        return $criteria_string;
    }

    public function saveSwitch($object1, $object2)
    {
        $em = $this->getDbProvider()->getEntityManager();
        $em->persist($object1);
        $em->persist($object2);
        $em->flush();
    }
}


