<?php

namespace Score\BaseBundle\Services;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerManager
{

    protected $mailer;
    protected $subject;
    protected $from = 'info@ewobox.sk';
    protected $to;
    protected $body;
    protected $logger;

    public function __construct($mailer, $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }


    public function getSubject()
    {
        return $this->subject;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function send($data)
    {

        $subject = array_key_exists('subject', $data) ? $data['subject'] : $this->getSubject();
        $to = array_key_exists('to', $data) ? $data['to'] : $this->getTo();
        $from = array_key_exists('from', $data) ? $data['from'] : $this->getFrom();
        $body = array_key_exists('body', $data) ? $data['body'] : $this->getBody();

        $email = (new Email())
            ->from($from)
            ->to($to)
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject($subject)
            //->text('Sending emails is fun again!')
            ->html($body);

    
        try {
            $this->mailer->send($email);
            return true;
        } catch (\Exception $e) {
            $this->logger->error('SEND EMAIL FAILED:' . $e);
            //file_put_contents(APPLICATION_PATH."/mail-fail.txt", date('d.m.Y H:i:s')." zlyhalo odslanie name ".$rec." \n", FILE_APPEND | LOCK_EX);
            return false;
        }
    }

}
