<?php

namespace Score\BaseBundle\Services;

class AdjacencyArrayTreeManager
{
    /*
     * Array of parents, usefull for breadcrumbing, path, etc.
     */
    private $parent_chain = array();
    //private $scalar;
    /**
     *current menu item
     * @var type
     */
    private $object;
    /*
     * Flat array of all objects
     */
    private $collection = [];

    public function getCollection()
    {
        return $this->collection;
    }

    public function setCollection($collection)
    {
        $this->collection = $collection;
    }

    /*public function setScalar($val)
    {
    $this->scalar = $val;
    }*/


    public function setObject($val)
    {
        $this->object = $val;
    }

    public function getObject()
    {
        return $this->object;
    }


    /**
     * @param mixed $id
     */
    public function hasChildren($id)
    {
        foreach ($this->getCollection() as $item) {
            if ($item->getParentId() == $id) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     *
     * @param mixed $id
     * @return array
     */
    public function getChildren($id)
    {
        $children = array();

        foreach ($this->getCollection() as $item) {
            if ($item->getParentId() == $id) {
                $index = ($item->getSortOrder() * 10000) + $item->getId();
                $children[$index] = $item;
            }
        }

        ksort($children);
        return $children;
    }

    public function getParent($object = null)
    {
        if (null == $object) {
            $object = $this->getObject();
        }

        foreach ($this->getCollection() as $item) {
            if ($item->getId() == $object->getParentId()) {
                return $item;
            }
        }


    }

    /**
     * @param object $object
     * @return array $parent_chain array of parent objects
     */
    public function getObjectParentChain($object = null)
    {
        $parent = $this->getParent($object);
        if (null != $parent) {
            $this->parent_chain[$parent->getId()] = $parent;
            $this->getObjectParentChain($parent);
        }
        return $this->parent_chain;
    }


    /**
     * Create multidimensional array
     *
     * @param mixed $id object id
     * @param $options options depth = depth of tree
     * @return array multidimensional array
     */
    public function buildTree($id, $options = ['depth' => 10])
    {
        $tmp = [];
        if ($this->hasChildren($id)) {
            $childs = $this->getChildren($id);

            if (count($childs) > 0) {
                foreach ($childs as $child) {
                    $tmp[$child->getId()]['node'] = $child;
                    if ($options['depth'] > $child->getLvl()) {
                        if ($this->hasChildren($child->getId())) {
                            $tmp[$child->getId()]['children'] = $this->buildTree($child->getId(), $options);
                        }
                    }
                }
            }
        }
        return $tmp;
    }


    /**
     * Create multidimensional array
     *
     * @param mixed $id object id
     * @param $full_path array of brancha ids, parent ids + current id
     * @param $options options depth = depth of tree
     * @return array multidimensional array
     */
    public function buildBranchTree($id, $full_path, $options = array('depth' => 100))
    {
        $tmp = array();
        if ($this->hasChildren($id)) {
            $childs = $this->getChildren($id);
            foreach ($childs as $child) {
                $tmp[$child->getId()]['node'] = $child;
                if ($options['depth'] > $child->getLvl() && in_array($child->getId(), $full_path)) {
                    if ($this->hasChildren($child->getId())) {
                        $tmp[$child->getId()]['children'] = $this->buildBranchTree($child->getId(), $full_path, $options);
                    }
                }
            }
        }
        return $tmp;
    }

    public function setRepository($r)
    {
        return $this;
    }


}


