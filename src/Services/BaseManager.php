<?php

namespace Score\BaseBundle\Services;

use \Doctrine\ORM\Tools\Pagination\Paginator;

/**
 *
 */
class BaseManager
{
    /*
     * Repository
     */
    protected $repositiory;


    /*
     * Storage provider
     */
    protected $db_provider;

    function __construct($em, $repository)
    {
        $this->db_provider = $em;
        $this->repositiory = $this->db_provider->getManager()->getRepository($repository);
    }


    public function getDbProvider()
    {
        return $this->db_provider;
    }

    public function setRepository($val)
    {
        $this->repositiory = $val;
    }

    public function getRepository()
    {
        return $this->repositiory;
    }

    public function convertEntityToArray($entityObject)
    {
        $data = array();

        $className = get_class($entityObject);
        $metaData = $this->db_provider->getEntityManager()->getClassMetadata($className);

        foreach ($metaData->fieldMappings as $field => $mapping) {
            $method = "get" . ucfirst($field);
            $data[$field] = call_user_func(array($entityObject, $method));
        }

        /*
        foreach ($metaData->associationMappings as $field => $mapping)
        {
            // Sort of entity object
            $object = $metaData->reflFields[$field]->getValue($entityObject);

            $data[$field] = $this->convertEntityToArray($object);
        }
        */
        return $data;
    }

    public function getObjectIdFromHash($hash)
    {
        $id = substr($hash, 8);
        $id = substr($id, 0, -8);
        $id = $id - 128;
        $id = $id / 4;
        return $id;
    }

    public function paginate($query, $settings)
    {
        $pageSize = $settings['pageSize'];
        $page = $settings['page'];
        // load doctrine Paginator
        $paginator = new Paginator($query);

        // you can get total items
        $totalItems = count($paginator);

        // get total pages
        $pagesCount = ceil($totalItems / $pageSize);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize); // set the limit
        $list = array();
        foreach ($paginator as $pageItem) {
            $list[] = $pageItem;
        }
        return ['list' => $list, 'total' => $totalItems, 'pagesCount' => $pagesCount];
    }


}


