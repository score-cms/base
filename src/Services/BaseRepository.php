<?php


namespace Score\BaseBundle\Services;

use Doctrine\ORM\EntityRepository;


class BaseRepository extends EntityRepository
{

    public function updateEntityFromArray($entity, $data)
    {
        $className = get_class($entity);

        $rules = $this->getEntityManager()->getClassMetadata($className)->fieldMappings;


        foreach ($rules as $rule) {
            $method_name_parts = explode('_', $rule['fieldName']);
            $method_name_parts = array_map('ucfirst', $method_name_parts);
            $method_name = 'set' . implode($method_name_parts);


            if (array_key_exists($rule['fieldName'], $data)) {
                $val = $data[$rule['fieldName']];

                if (method_exists($entity, $method_name)) {
                    if ($rule['type'] == 'datetime') {
                        if (null != $val) {
                            $val = new \DateTime($val);
                        } else {
                            //pretypovanie na null kvoli ===
                            //$val = null;
                        }
                    }


                    if ($rules['type'] == 'boolean') {
                        $val = (1 == $val) ? true : false;
                    }

                    call_user_func_array(array($entity, $method_name), array($val));
                }
            }


        }


    }
}
