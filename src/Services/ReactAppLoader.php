<?php


namespace Score\BaseBundle\Services;


class ReactAppLoader
{

    private $rootDir = null;

    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    private $apps =
        [
            "form" => 'bundles/scorecms/react/form/build',
            "gallery" => 'bundles/scorecms/react/gallery/build',
            "files" => 'bundles/scorecms/react/files/build'
        ];

    public function getFilePaths($extension, $pathToManifest, $allExtensions = false)
    {
        //$path = 'indicators/build';
        //$extension = ".js";
        $manifestPath = $pathToManifest . '/asset-manifest.json';
        $manifestFile = fopen($manifestPath, "r");
        $content = fread($manifestFile, filesize($manifestPath));
        fclose($manifestFile);

        $result = [];
        foreach (json_decode($content, true)['entrypoints'] ?? [] as $fileName) {
            if ($allExtensions || (mb_strrpos($fileName, $extension) === mb_strlen($fileName) - mb_strlen($extension))) {
                $result[] = "/" . $pathToManifest . "/" . $fileName;
            }
        }
        return ($result);
    }

    public function getFilePathsDist($pathToDist, $extension = null)
    {
        $root = $this->rootDir;
        $dir = $pathToDist;

        $subdir = 'dist/assets';
        $directory = $root . '/public/' . $dir . '/' . $subdir . '/';

        try {
            $files = scandir($directory);
            $files = array_diff($files, ['.', '..']);
        } catch (\Exception $e) {
            $files = [];
        }

        $result = [];
        foreach ($files as $fileName) {
            if (!$extension || (mb_strrpos($fileName, $extension) === mb_strlen($fileName) - mb_strlen($extension))) {
                $result[] = "/" . $dir . '/' . $subdir. "/" . $fileName;
            }
        }
        return $result;
    }

    public function findAppsByBlockNames($blockNames = [])
    {
        $appNames = array_keys($this->apps);
        $result = [];
        foreach (array_unique($blockNames) as $blockName) {
            if (in_array($blockName, $appNames)) {
                $newFiles = $this->getFilePaths(null, $this->apps[$blockName], true);
                $result = array_merge($result, $newFiles);
            }
        }
        return ($result);
    }
}
