<?php

namespace Score\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{


    /**
     * @Route("/dummy", name="dummy")
     */
    public function dummyAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('GalleryBundle:Gallery');
        $imageManager = $this->container->get('score.manager.image');
        $list = $repository->findAll();


        foreach ($list as $gallery) {
            $galleryDir = $this->getParameter('gallery_upload_directory') . '/' . $gallery->getId();
            foreach ($gallery->getPhoto() as $photo) {
                $item = $photo->getFileName();

                if (file_exists($this->getParameter('gallery_upload_directory') . '/tmp/' . $item)) {
                    copy($this->getParameter('gallery_upload_directory') . '/tmp/' . $item, $galleryDir . '/' . $item);

                    $fileThumb100Name = 'thumb_100x100_' . $item;
                    $fileThumb200Name = 'thumb_200x160_' . $item;
                    $fileThumb640Name = 'thumb_640x480_' . $item;
                    $fileThumb1200Name = 'thumb_1200x960_' . $item;

                    $imageManager->createThumb($galleryDir . '/' . $item, $galleryDir . '/' . $fileThumb100Name, array('width' => 100, 'height' => 100));
                    $imageManager->createThumb($galleryDir . '/' . $item, $galleryDir . '/' . $fileThumb200Name, array('width' => 200, 'height' => 160));
                    $imageManager->createThumb($galleryDir . '/' . $item, $galleryDir . '/' . $fileThumb640Name, array('width' => 640, 'height' => 480));
                    $imageManager->createThumb($galleryDir . '/' . $item, $galleryDir . '/' . $fileThumb1200Name, array('width' => 1200, 'height' => 960));
                }
            }
        }


        return $this->render('ScoreBaseBundle:Default:dummy.html.twig');
    }


}
