<?php

namespace Score\BaseBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class AdminController extends AbstractController
{

    protected $objectRepository;
    protected $object;

    protected function getDatagridDraw($session)
    {

        if (null == $session->get('datatableDraw')) {
            $datatableDraw = 0;
        } else {
            $datatableDraw = $session->get('datatableDraw');
        }
        $session->set('datatableDraw', ++$datatableDraw);
        return $session->get('datatableDraw');
    }

    protected function getKeywordsFormChoices($manager)
    {
        $availableKeywords = $manager->getAvailableKeywords();
        foreach ($availableKeywords as $keyword) {
            $keywordsFormChoices[$keyword->getName()] = $keyword;
        }

        return $keywordsFormChoices;
    }

    public function getKeywordForm($manager, $object)
    {
        $keywordsFormChoices = $this->getKeywordsFormChoices($manager);


        $attachedKeywords = array();
        foreach ($manager->getKeywords($object) as $attachedKeyword) {
            $attachedKeywords[] = $attachedKeyword->getKeyword();
        }


        $keywordsForm = $this->createForm(\KeywordBundle\Form\ItemKeywordsForm::class, array('keywords' => $attachedKeywords), array('keywordChoices' => $keywordsFormChoices));
        return $keywordsForm;
    }


}
