<?php

namespace Score\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{

     /**
     * @Route("/test/email", name="tets_email")
     */
    public function testEmailAction()
    //public function testEmailAction(\Swift_Mailer $mailer) TODO
    {

        //$mesage = $mailer->createMessage();


        $message = (new \Swift_Message('Hello Email'))
                ->setFrom('info@sazp.sk')
                ->setTo('marek.hubacek@sazp.sk')
                ->setBody('test', 'text/html'
                )

        ;

        $result = $mailer->send($message);

        return $this->render('@ScoreBase/Test/result.html.twig', array('result' => $result));
    }
}
