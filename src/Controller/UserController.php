<?php

namespace Score\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserController extends AbstractController {

    protected $objectRepository;
    protected $object;



    protected function getKeywordsFormChoices($manager)
    {
        $availableKeywords = $manager->getAvailableKeywords();
        foreach ($availableKeywords as $keyword)
        {
            $keywordsFormChoices[$keyword->getName()] = $keyword->getId();
        }

        return $keywordsFormChoices;
    }

    public function getKeywordForm($manager, $object)
    {
        $keywordsFormChoices = $this->getKeywordsFormChoices($manager);


        $attachedKeywords = array();
        foreach ($manager->getKeywords($object) as $attachedKeyword)
        {
            $attachedKeywords[] = $attachedKeyword->getKeyword()->getId();
        }


        $keywordsForm = $this->createForm(\KeywordBundle\Form\ItemKeywordsForm::class, array('keywords' => $attachedKeywords), array('keywordChoices' => $keywordsFormChoices));
        return $keywordsForm;
    }

    protected function persistLocality($localityItem, $type, $object, $localityData)
    {
        $em = $this->getDoctrine()->getManager();
        $locality = null;
        if ($type == 'UserLocality')
        {
            $newlocalityItem = new \MapBundle\Entity\UserLocality();
        }

        if ($type == 'OrganizationLocality')
        {
            $newlocalityItem = new \MapBundle\Entity\OrganizationLocality();
        }
        if ($type == 'EventLocality')
        {
            $newlocalityItem = new \MapBundle\Entity\EventLocality();
        }

        if (null != $localityData['name'])
        {
            if (null == $localityItem)
            {
                $localityItem = $newlocalityItem;
                $locality = new \MapBundle\Entity\Locality();
            }
            else
            {
                $locality = $localityItem->getLocality();
            }
            $locality->setName($localityData['name']);


            if (null != $localityData['json_definition'])
            {
                $locality->setMapData($localityData['json_definition']);
                $locality->setLat($localityData['lat']);
                $locality->setLng($localityData['lng']);
            }
        }
        else
        {
            //remove locality
            if (null != $localityItem)
            {
                $em->remove($localityItem);
                $em->flush();

                if ($type == 'UserLocality')
                {
                    $object->setUserLocality(null);
                }
                //$em->persist($localityItem);
            }
        }

        if (null != $locality)
        {
            $em->persist($locality);
            $localityItem->setLocality($locality);


            if ($type == 'UserLocality')
            {
                $localityItem->setUser($object);
                $object->setUserLocality($localityItem);
            }

            if ($type == 'OrganizationLocality')
            {
                $localityItem->setOrganization($object);
            }


            if ($type == 'EventLocality')
            {
                $localityItem->setEvent($object);
            }

            $em->persist($localityItem);
        }
    }

    public function checkPermission($user, $permissionCode, $options)
    {
        if ('organizationAdmin' == $permissionCode)
        {
            $organizationUser = $options['organization']->getOrganizationUserByUser($user);
            if ($organizationUser->getUserRole() != 'ADMIN')
            {
                //throw $this->createAccessDeniedException();
                return $this->redirectToRoute('login');
            }
        }

        if ('editArticle' == $permissionCode or 'deleteArticle' == $permissionCode)
        {
            if ($options['article']->getCreatedBy() != $user->getId())
            {
                //check if organization admin
                $organizationManager = $this->container->get('organization.manager');
                $userOrganizations = $organizationManager->getUserOrganizations($user);
                $isOrganizationAdmin = false;
                foreach ($userOrganizations as $userOrganization)
                {
                    if ($userOrganization->getOrganization()->getId() == $options['article']->getOrganizationId() and $userOrganization->getUserRole() == 'ADMIN')
                    {
                        return true;
                    }
                }
                throw $this->createAccessDeniedException();
            }
        }

        if ('editEvent' == $permissionCode or 'deleteEvent' == $permissionCode)
        {
            if ($options['event']->getCreatedBy() != $user->getId())
            {
                //check if organization admin
                $organizationManager = $this->container->get('organization.manager');
                $userOrganizations = $organizationManager->getUserOrganizations($user);
                $isOrganizationAdmin = false;
                foreach ($userOrganizations as $userOrganization)
                {
                    if ($userOrganization->getOrganization()->getId() == $options['event']->getOrganizationId() and $userOrganization->getUserRole() == 'ADMIN')
                    {
                        return true;
                    }
                }
                throw $this->createAccessDeniedException();
            }
        }

        if ('editPromoteCall' == $permissionCode or 'deletePromoteCall' == $permissionCode)
        {
            if ($options['call']->getCreatedBy() != $user->getId())
            {
                //check if organization admin
                $organizationManager = $this->container->get('organization.manager');
                $userOrganizations = $organizationManager->getUserOrganizations($user);
                $isOrganizationAdmin = false;
                foreach ($userOrganizations as $userOrganization)
                {
                    if ($userOrganization->getOrganization()->getId() == $options['call']->getOrganizationId() and $userOrganization->getUserRole() == 'ADMIN')
                    {
                        return true;
                    }
                }
                throw $this->createAccessDeniedException();
            }
        }

        if ('editSource' == $permissionCode or 'deleteSource' == $permissionCode)
        {
            if ($options['source']->getCreatedBy() != $user->getId())
            {
                //check if organization admin
                $organizationManager = $this->container->get('organization.manager');
                $userOrganizations = $organizationManager->getUserOrganizations($user);
                $isOrganizationAdmin = false;
                foreach ($userOrganizations as $userOrganization)
                {
                    if ($userOrganization->getOrganization()->getId() == $options['source']->getOrganizationId() and $userOrganization->getUserRole() == 'ADMIN')
                    {
                        return true;
                    }
                }
                throw $this->createAccessDeniedException();
            }
        }
        if ('editOrganizationActivity' == $permissionCode or 'deleteOrganizationActivity' == $permissionCode)
        {
            if ($options['activity']->getCreatedBy() != $user->getId())
            {
                //check if organization admin
                $organizationManager = $this->container->get('organization.manager');
                $userOrganizations = $organizationManager->getUserOrganizations($user);
                $isOrganizationAdmin = false;
                foreach ($userOrganizations as $userOrganization)
                {
                    if ($userOrganization->getOrganization()->getId() == $options['activity']->getOrganizationId() and $userOrganization->getUserRole() == 'ADMIN')
                    {
                        return true;
                    }
                }
                throw $this->createAccessDeniedException();
            }
        }
    }

    protected function handleFormRequest($form, $request)
    {
        $user = $this->getUser();

        $object = $form->getData();
        $form->handleRequest($request);

        if ($user->hasLevel('ADVANCED') and ! $user->hasLevel('EXPERT'))
        {
            $organizationManager = $this->container->get('organization.manager');
            $object->setOrganizationId($organizationManager->getDefaultOrganizationId());
        }

        if($object->getOrganizationId() == null)
        {
            $organizationManager = $this->container->get('organization.manager');
            $object->setOrganizationId($organizationManager->getDefaultOrganizationId());
        }
    }

    public function handleGalleryRequest(Request $request, $object)
    {
        $em = $this->getDoctrine()->getManager();
        $galleryRepository = $em->getRepository('GalleryBundle:Gallery');
        $imageManager = $this->container->get('score.manager.image');
        $galleryId = $request->get('gallery_id');
        $items = $request->get('files');
        if (null !=$items && count($items) > 0)
        {
            if (null == $galleryId)
            {
                $gallery = new \GalleryBundle\Entity\Gallery();
                $gallery->setName('Gallery');
                $em->persist($gallery);


                if (is_a($object, '\ArticleBundle\Entity\Article'))
                {
                    $galleryItem = new \ArticleBundle\Entity\GalleryItemArticle();
                    $galleryItem->setArticle($object);
                }

                $galleryItem->setGallery($gallery);

                $em->persist($galleryItem);
                $em->flush();
            }
            else
            {
                $gallery = $galleryRepository->find($galleryId);
                //remove all photos
                foreach ($gallery->getPhoto() as $photo)
                {
                    $em->remove($photo);
                }
                $em->flush();
            }

            $galleryDir = $this->getParameter('gallery_upload_directory') . '/' . $gallery->getId();
            if (!file_exists($galleryDir))
            {
                mkdir($galleryDir);
            }

            foreach ($items as $key => $item)
            {
                $galleryPhoto = new \GalleryBundle\Entity\GalleryPhoto();
                $galleryPhoto->setSortOrder($key + 1);
                $galleryPhoto->setName('Photo');
                $galleryPhoto->setFileName($item);
                $galleryPhoto->setGallery($gallery);
                $em->persist($galleryPhoto);

                if (file_exists($this->getParameter('gallery_upload_directory') . '/tmp/' . $item))
                {
                    copy($this->getParameter('gallery_upload_directory') . '/tmp/' . $item, $galleryDir . '/' . $item);

                    $fileThumb100Name = 'thumb_100x100_' . $item;
                    $fileThumb200Name = 'thumb_200x160_' . $item;
                    $fileThumb640Name = 'thumb_640x480_' . $item;
                    $fileThumb1200Name = 'thumb_1200x960_' . $item;

                    $imageManager->createThumb($galleryDir . '/' . $item, $galleryDir . '/' . $fileThumb100Name, array('width' => 100, 'height' => 100));
                    $imageManager->createThumb($galleryDir . '/' . $item, $galleryDir . '/' . $fileThumb200Name, array('width' => 200, 'height' => 160));
                    $imageManager->createThumb($galleryDir . '/' . $item, $galleryDir . '/' . $fileThumb640Name, array('width' => 640, 'height' => 480));
                    $imageManager->createThumb($galleryDir . '/' . $item, $galleryDir . '/' . $fileThumb1200Name, array('width' => 1200, 'height' => 960));
                }
            }
            $em->flush();
        }
        else
        {
            if (null != $galleryId)
            {
                $gallery = $galleryRepository->find($galleryId);
                //remove all photos
                foreach ($gallery->getPhoto() as $photo)
                {
                    $em->remove($photo);
                }
                $em->flush();
            }
        }
    }

    public function getGenerateSlug($name, $repoName)
    {
        $em = $this->getDoctrine()->getManager();
        $seoUrlService = $this->container->get('score.manager.seoUrl');
        $slug = $seoUrlService->generateSeoString($name);
        //check if slug exists
        $existSlug = $em->getRepository($repoName)->findBy(array('name' => $name));
        if (count($existSlug) > 0)
        {
            $slug = $slug . '-' . count($existSlug);
        }

        return $slug;
    }

    public function getOrganizationChoices($user)
    {
        $organizationManager = $this->container->get('organization.manager');
        $existUserOrganizations = $organizationManager->getUserOrganizations($user);
        $organizationChoices = array('' => '');
        foreach ($existUserOrganizations as $existUserOrganization)
        {
            $choiceName = $existUserOrganization->getOrganization()->getName();
            if ($existUserOrganization->getOrganization()->getParentOrganization() != null)
            {
                $choiceName = $existUserOrganization->getOrganization()->getParentOrganization()->getName() . ' / ' . $choiceName;
            }
            $organizationChoices[$choiceName] = $existUserOrganization->getOrganization()->getId();
        }
        return $organizationChoices;
    }

    public function handleActivityContentRequest(Request $request,$object)
    {
        $em = $this->getDoctrine()->getManager();
        $activityRepo = $em->getRepository('OrganizationBundle:Activity');
        $activityContentRepo = $em->getRepository('OrganizationBundle:ActivityContent');

        //remove old
        $existContent = $activityContentRepo->findBy(array('itemId' => $object->getId()));
        foreach($existContent as $existContentItem)
        {
            $em->remove($existContentItem);
            $em->flush();
        }

        $activities = $request->get('activity_content');
        if(null != $activities)
        {
            foreach($activities as $activityId)
            {
                $activity = $activityRepo->find($activityId);

                if(is_a($object, '\ArticleBundle\Entity\Article'))
                {
                    $activityContent = new \OrganizationBundle\Entity\ActivityArticle();
                    $activityContent->setArticle($object);
                }

                if(is_a($object, '\EventBundle\Entity\Event'))
                {
                    $activityContent = new \OrganizationBundle\Entity\ActivityEvent();
                    $activityContent->setEvent($object);
                }

                if(is_a($object, '\SourceBundle\Entity\Source'))
                {
                    $activityContent = new \OrganizationBundle\Entity\ActivitySource();
                    $activityContent->setSource($object);
                }



                $activityContent->setActivity($activity);
                $em->persist($activityContent);
                $em->flush();
            }
        }

    }


}
