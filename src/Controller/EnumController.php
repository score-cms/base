<?php

namespace Score\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Score\BaseBundle\Services\EnumManager;

class EnumController extends AbstractController {

    /**
     * @Route("/enum/ajax/district", name="enum_ajax_district")
     */
    public function ajaxDistrictAction(Request $request, EnumManager $enumManager)
    {
         $choices = $enumManager->getDistrictFormChoices($request->get('regionId'));
          $choices = array_merge(array( $this->get('translator')->trans('district.choice.placeholder') => ''),$choices);
          return $this->json($choices);
    }
    /**
     * @Route("/enum/ajax/city", name="enum_ajax_city")
     */
    public function ajaxCityAction(Request $request, EnumManager $enumManager)
    {
         $choices = $enumManager->getCityFormChoices($request->get('districtId'));
          $choices = array_merge(array( $this->get('translator')->trans('city.choice.placeholder') => ''),$choices);
          return $this->json($choices);
    }

}
