<?php

namespace Score\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{

    /**
     * @Route("/api/token",name="api_token")
     */
    public function tokenAction(Request $request)
    {
        $clientId = $request->get('key');
        $em = $this->getDoctrine()->getManager();
        $apiAuth = $em->getRepository('ScoreBaseBundle:ApiAuth')->findOneBy(array('key' => $clientId));

        $currentDate = new \DateTime();

        if ($apiAuth->getTokenExpiration()->getTimestamp() < $currentDate->getTimestamp()) {
            $currentDate->modify('+ 30 minutes');
            $token = sha1(serialize($apiAuth) . time() . rand(1, 100));
            $apiAuth->setToken($token);
            $apiAuth->setTokenExpiration($currentDate);
            $em->persist($apiAuth);
            $em->flush($apiAuth);
        }

        $responseData = ['status' => 'SUCCESS', 'token' => $apiAuth->getToken()];
        $response = new JsonResponse($responseData);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        return $response;
    }

    public function entityToArray($className, $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $meta = $em->getClassMetadata('\OrganizationBundle\Entity\Organization');
        $data = [];
        foreach ($meta->fieldNames as $fieldName => $method) {
            $data[$fieldName] = call_user_func(array($entity, 'get' . ucfirst($method)));
        }
        return $data;
    }

    public function jsonResponse($data)
    {
        header('Access-Control-Allow-Origin: *');
        $response = new JsonResponse($data);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        return $response;
    }

}
