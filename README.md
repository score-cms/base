# score/BaseBundle #

SAZP CMS

_This bundle is only one part of the Score CMS._

[tutorial for installing complete Score CMS](https://bitbucket.org/score-cms/cms/ "README.md")

### What is this repository for? ###

* Part of Score CSM
* Version: __2.1.0__

### How do I get set up? ###

* add repository to `composer.json`
  
    ``` 
        "repositories": [
            {
                "type": "vcs",
                "url": "git@bitbucket.org:score-cms/base.git"
            }  
        ]
    ```


* Install score/BaseBundle
    ```
    composer require score/base
    ```

  
* add routes to `config/routes/annotations.yaml`
  ```
  score_base:
      resource: "@ScoreBaseBundle/Controller/"
      prefix: /
      type: annotation
  ```


### Authors ###
* Marek Hubáček
* Jozef Hort
